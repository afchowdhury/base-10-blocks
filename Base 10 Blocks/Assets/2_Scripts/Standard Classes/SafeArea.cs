﻿using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class SafeArea : MonoBehaviour
{
    
    
    [SerializeField] private bool ConformY = false;
    [SerializeField] private bool ConformX = false;
    #if !UNITY_WEBGL
    private RectTransform _panel;
    private Rect _lastSafeArea = new Rect(0, 0, 0, 0);

    void Awake()
    {
        _panel = GetComponent<RectTransform>();
    }

    private void Start()
    {
        Refresh();

        //HACK - Removed events to refresh safe area as this was breaking the safe area on older android devices.
        //Refresh(ScreenOrientationManager.Instance.Orientation);
        //ScreenOrientationManager.Instance.OnScreenOrientationChanged -= Refresh;
        //ScreenOrientationManager.Instance.OnScreenOrientationChanged += Refresh;
    }

    private void OnDestroy()
    {
        //HACK - Removed events to refresh safe area as this was breaking the safe area on older android devices.
        //if (ScreenOrientationManager.Instance != null)
        //{
        //    ScreenOrientationManager.Instance.OnScreenOrientationChanged -= Refresh;
        //}
    }

    private void Update()
    {
        Refresh();
    }

    void Refresh()
    {
        Rect safeArea;

        if (Application.isEditor)
        {
            safeArea = TestGetSafeArea();
        }
        else
        {
            safeArea = GetSafeArea();
        }

        if (safeArea != _lastSafeArea)
        {
            ApplySafeArea(safeArea);
        }
    }

    Rect GetSafeArea()
    {
        return Screen.safeArea;
    }

    void ApplySafeArea(Rect r)
    {
        _lastSafeArea = r;

        Vector2 originalAnchorMin = _panel.anchorMin;
        Vector2 originalAnchorMax = _panel.anchorMax;

        // Convert safe area rectangle from absolute pixels to normalised anchor coordinates
        Vector2 anchorMin = r.position;
        Vector2 anchorMax = r.position + r.size;

        anchorMin.x /= Screen.width;
        anchorMin.y /= Screen.height;
        anchorMax.x /= Screen.width;
        anchorMax.y /= Screen.height;

        if (!ConformY)
        {
            anchorMin.y = originalAnchorMin.y;
            anchorMax.y = originalAnchorMax.y;
        }

        if (!ConformX)
        {
            anchorMin.x = originalAnchorMin.x;
            anchorMax.x = originalAnchorMax.x;
        }

        _panel.anchorMin = anchorMin;
        _panel.anchorMax = anchorMax;
    }

#endif

    #region Editor Test

    public enum SimDevice
    {
        None,
        iPhoneX
    }

    public SimDevice Sim;

    Rect[] NSA_iPhoneX = new Rect[]
    {
        new Rect(0f, 102f / 2436f, 1f, 2202f / 2436f), // Portrait
        new Rect(132f / 2436f, 63f / 1125f, 2172f / 2436f, 1062f / 1125f) // Landscape
    };

    Rect TestGetSafeArea()
    {
        Rect safeArea = Screen.safeArea;

        if (Application.isEditor && Sim != SimDevice.None)
        {
            Rect nsa = new Rect(0, 0, Screen.width, Screen.height);

            switch (Sim)
            {
                case SimDevice.iPhoneX:
                    if (Screen.height > Screen.width) // Portrait
                        nsa = NSA_iPhoneX[0];
                    else // Landscape
                        nsa = NSA_iPhoneX[1];
                    break;
                default:
                    break;
            }

            safeArea = new Rect(Screen.width * nsa.x, Screen.height * nsa.y, Screen.width * nsa.width, Screen.height);
        }

        return safeArea;
    }

    #endregion
}