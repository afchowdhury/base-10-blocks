﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SegmentManager : MonoBehaviour
{
    public Button button;
    public GameObject container;
    public int segmentAmount, maxSegments, yellowCount, greenCount;
    private bool isYellow;
    public GameObject fraction, ratio, decimalDisplay, percentage;
    public Text numerator, denominator;
    public Text yellowRatio, greenRatio;
    public float decimalFloat;
    public float percentageDisplay;

    public int buttonLoop;
    
    public void Start()
    {
        segmentAmount = 1;
        greenCount = 1;
        yellowCount = 0;

        buttonLoop = 1;
        SwitchSolution();

        ShowFraction();
        ShowRatio();
        ShowDecimal();
        ShowPercentage();
    }
    
    public void ButtonPress()
    {
        if (segmentAmount < maxSegments)
        {

        Instantiate(button,container.transform);
        segmentAmount ++;
        greenCount ++;

        }
        ShowFraction();
        ShowRatio();
        ShowDecimal();
        ShowPercentage();

    }

    public void subtractSegment()
    {
        if (segmentAmount > 1)
        {
            isYellow = container.GetComponent<Transform>().GetChild(segmentAmount-1).gameObject.GetComponent<ColourChange>().activated;
            if(!isYellow)
            {
                Destroy(container.GetComponent<Transform>().GetChild(segmentAmount-1).gameObject);
                segmentAmount --;
                greenCount --;

            }
            else
            {
                Destroy(container.GetComponent<Transform>().GetChild(segmentAmount-1).gameObject);
                segmentAmount --;
                yellowCount --;

            }

            ShowFraction();
            ShowRatio();
            ShowDecimal();
            ShowPercentage();
        }
    }

    public void Update()
    {
        //colourChangesList.GetComponent<transform>();
    }

    public void ShowFraction()
    {
        numerator.text = yellowCount.ToString();
        denominator.text = segmentAmount.ToString();
    }

    public void ShowRatio()
    {
        yellowRatio.text = yellowCount.ToString();
        greenRatio.text = greenCount.ToString();

    }

    public void ShowDecimal()
    {
        decimalFloat = ((float)yellowCount/(float)segmentAmount);
        decimalDisplay.GetComponent<Text>().text = decimalFloat.ToString("F3");

    }

    public void ShowPercentage()
    {
        percentageDisplay = (((float)yellowCount/(float)segmentAmount)*100);
        percentage.GetComponent<Text>().text = (percentageDisplay.ToString("F1") + "%");

    }
  
    public void SwitchSolution()
    {
        switch(buttonLoop)
        {
            case 1:
                buttonLoop++;
                fraction.SetActive(true);
                percentage.SetActive(false);

            break;

            case 2:
                buttonLoop++;
                ratio.SetActive(true);
                fraction.SetActive(false);
            break;

            case 3:
                buttonLoop++;
                decimalDisplay.SetActive(true);
                ratio.SetActive(false);
            break;

            case 4:
                buttonLoop = 1;
                percentage.SetActive(true);
                decimalDisplay.SetActive(false);
            break;
        }
    }
}
