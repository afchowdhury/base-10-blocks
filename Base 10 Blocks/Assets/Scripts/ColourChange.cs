﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColourChange : MonoBehaviour

{

    public Color yellow;
    public bool activated;
    public Image segment;
    public Color green;
    public SegmentManager segmentManager;

    public void Start()
    {
        segment.color = green;
        activated = false;
    }
    
    public void ColorSwitch()
    {
        if (!activated)
        {
            segment.color = yellow;
            activated = true;
            segmentManager.yellowCount ++;
            segmentManager.greenCount --;

        }
        else
        {
            segment.color = green;
            activated = false;
            segmentManager.yellowCount --;
            segmentManager.greenCount ++;

        }

        segmentManager.ShowFraction();
        segmentManager.ShowRatio();
        segmentManager.ShowDecimal();
        segmentManager.ShowPercentage();
    }

}
